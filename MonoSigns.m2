newPackage(
	"MonoSigns",
	Version => "0.2",
	Date => "March 2020",
	Authors => {{
		  Name => "Alexandru Iosif",
		  Email => "iosif@aices.rwth-aachen.de",
		  HomePage => "https://alexandru-iosif.github.io"}},
    	Headline => "A package for reducing polynomial systems of
    equations with nonnegative solutions",
	AuxiliaryFiles => false,    
    	PackageImports => {"Elimination"},
	DebuggingMode => true --set to false once the package is in finished form
)

export {
     -- 'Official' functions
     "reducedRowEchelon",
     "monoSignsRows",
     "reduceSystemOnce",
     "reduceSystemFull"

     -- Not in the interface:
     
     -- Not yet built:
     -- upperLowerTriangular,
}

reducedRowEchelon = A ->(
    --this function returns the row echelon form of a matrix it is
    --taken from the Macaulay2 package Binomials of Thomas Kahle (that
    --A. Iosif jointed in 2018)
    --NB: here entries of the output live in
    --the fraction field of the entries of the input. This should
    --eventually become an option in te interface
    if A == 0 then return A;
    c := numColumns A;
    variable := symbol variable;
    R:=ring (A_(0,0)); S := R[variable_1..variable_c,MonomialOrder => Lex];
    A=sub(transpose(coefficients(matrix {flatten entries gens gb ideal flatten entries (sub(A,S)*transpose(vars S))},
		Monomials=> flatten entries vars S))_1,R);
    matrix reverse entries A)

-- upperLowerTriangular = A->(
--     --this is a function which converts a matrix into its upper-lower triangular form:
--     --I think that, this form should be the one that contains the largest number of non-negative rows
--     )

monoSignsRows = A ->(
    cRow := symbol cRow;
    bcRow := symbol bcRow;
    nN := {};
    R := RR;
    AR := substitute (A,R);
    for i from 0 to numrows AR - 1 do(
    	cRow = toList set flatten entries AR^{i};
    	cRow = cRow - set{0_R};
    	bcRow = set apply(cRow, j -> j < 0);
	if toList bcRow == {false} or toList bcRow == {true} then( 
	    nN = nN | {flatten entries A^{i}};
	    );
	);
    if nN == {} then nN = {{}};
    matrix nN
    )

-- findNonNegative = A ->(
--     --This function takes a Matrix and tries to find a linear
--     --combination of its row that is nonnegative.
--     )

reduceSystemOnce = (A,R) ->(
    --R should not contain the parameters as variables. Instead use R
    --= K[variables], where K = frac(QQ[parameters]).
    --
    --This function takes as an input a one column
    --matrix A whose rows are polynomials in R.  Then it supposes that
    --the solutions of this system and the eventual parameters from
    --the coefficient field of R are positive and nonnegative,
    --respectively. It tries to find linear combinations of these
    --polynomials which are either nonnegative or nonpositive. Then,
    --if the parameters are positive and the solution are nonnegative,
    --then each monomial from such a sum has tu be zero. The primary
    --decomposition of the resulting monomial ideal will give branches
    --in which some variables are zero.  This function performs this
    --reduction once. The output is a list {(A_i,R_i)}, where A_i
    --represents a matrix with the new system that lives in the
    --reduced ring R_i (that this R - variables that are zero on this
    --branch).

    if numColumns A != 1 then return "Error: the first entry should be a matrix with 1 column.";
    K := coefficientRing R;
    variablesR := vars R;
    A = transpose sub(A,R);
    (M,C) := coefficients A;
    M = transpose M;
    C = reducedRowEchelon transpose C; -- make it an option to first compute the reducedRowEchelon
    nonNegativeC := sub(monoSignsRows C, R);
    zeroMonomials := toList(set flatten entries monomials (nonNegativeC*M) - set{0_R});
    print zeroMonomials;
    zeroVariableIdeals := associatedPrimes sub(ideal zeroMonomials, QQ[flatten entries variablesR]);
    print zeroVariableIdeals;
    L := {};
    zeroVariables := symbol zeroVariables;
    RI := symbol RI;
    nonZeroVariables := symbol nonZeroVariables;
    newA := symbol newA;
    newSystem := symbol newSystem;

    for I in zeroVariableIdeals do(
    	use R;
    	zeroVariables = set flatten entries sub(gens I,R);
    	nonZeroVariables = toList(set flatten entries variablesR - zeroVariables);
    	if nonZeroVariables == {} then RI = QQ else RI = K[nonZeroVariables];
    	newSystem = sub(A,RI);
    	newSystem = set flatten entries newSystem - set {0_RI};
    	newSystem = matrix{toList newSystem};
	newSystem = sub(newSystem,R);
	nonZeroVariables = sub(matrix{nonZeroVariables},R);
	zeroVariables = sub(matrix{toList zeroVariables},R);
	if nonZeroVariables == 0 then RI = QQ else RI = K[flatten entries nonZeroVariables];	
    	L = L|{{newSystem,RI,zeroVariables}};
    	);
    L)


reduceSystemFull = (A,R) ->(
    -- This does the same as reduceSystemOnce recursively, until no
    -- change is detected. Note that the recursion has several
    -- branches.
    
    --It still needs some options.

    if numColumns A != 1 then return "Error: the first entry should be a matrix with 1 column.";

    nonZeroVariables := vars R;
    
    -- Next variable denotes the output. At the end it will be list of branches.
    LL := {};
    
    -- Next variable is used by next "while" and it denotes braches
    -- where computations have to still be performed by the
    -- corresponding "for". After each "for" it is updated, until it
    -- is empty.
    LL' := {{A,R,nonZeroVariables}};
    
    -- Auxiliary variables:
    L := symbol L;	  
    LL'' := symbol LL'';

    while LL' != {} do(
    	LL'' = {}; -- Auxiliary variable where we store the value of LL'.
    	for ll in LL' do(
    	    L = reduceSystemOnce(ll_0,ll_1); --ll_0 is a matrix and ll_1 is a ring.
    	    for l in L do(
    	    	-- Next condition moves finished branches into LL and
    	    	-- updates LL' with branches for which computations
    	    	-- need to continue.
		if l_0 == 0 or l_1 === QQ or l_2 == 0  then LL = LL|{{l_0,l_1, matrix{ toList (set flatten entries vars R - set flatten entries sub(vars l_1,R))} }} else LL'' = LL''|{l};
		);    
   	    LL' = LL'';
    	    );
	);
    LL = toList set apply (LL, i -> {sub(i_0,R),i_2});
    LL = apply (LL, i -> {i_0, if i_1 == vars R then QQ else QQ[toList(set flatten entries vars R - set flatten entries i_1)] ,i_1});
    LL)


beginDocumentation()

document {
    Key => MonoSigns,
    Headline => "a package for reducing polynomial systems of
    equations with nonnegative solutions",    
    EM "MonoSigns", " is a package that takes a polynomial system
    with positive solutions and looks for nonnegative or nonpositive
    polynomials among the equations of the system.",
    }

document {
     Key => {reducedRowEchelon},
     Headline => "the reduced row echelon form of a matrix",
     Usage => "reducedRowEchelon A",
     Inputs => {
          "A" => { "a matrix"} },
     Outputs => {
          {"the reduced row echelon form of A, where zero rows are removed"} },
     EXAMPLE {
          "A = matrix{{1,2},{2,1}}",
          "reducedRowEchelon A",
          },
     Caveat => {"The entries of the output lie in the fraction field of the ring of A. I plan to make this an option."}}

document {
     Key => {monoSignsRows},
     Headline => "the nonnegtive or nonpositive rows of a matrix",
     Usage => "monoSignsRows A",
     Inputs => {
          "A" => { "a matrix"} },
     Outputs => {
          {"the submatrix of A corresponding to the nonnegative or nonpositive rows of A"} },
     EXAMPLE {
          "A = matrix{{1,0},{0,1},{-1,0},{0,-1},{1,-1}}",
          "monoSignsRows A",
          }}


----- TESTS -----
TEST ///--test for reducedRowEchelon
assert (reducedRowEchelon matrix{{1,2},{2,1}} == matrix{{1_QQ,0_QQ},{0_QQ,1_QQ}})
assert (reducedRowEchelon matrix{{1,1,0},{1,2,1}} == matrix{{1_QQ,0_QQ,-1_QQ},{0_QQ,1_QQ,1_QQ}})
///

TEST ///--test for reduceSystemFull
R = QQ[x,y,z]
A = matrix{{x+y},{x+z}}
assert ((reduceSystemFull(A,R))_0 == 0)
assert ((reduceSystemFull(A,R))_1 === QQ)
assert ((reduceSystemFull(A,R))_2 == vars R)
///

end

viewHelp monoSignsRows
check "MonoSigns"


R = QQ[x,y,z];
M = matrix{{x*y+z}};
reduceSystemOnce (M,R)


    
R = QQ[x,y,z,MonomialOrder=>{Weights => {1,0,1}}] -- this gives me the upper-lower triangular form
gens gb ideal(x+y,x+2*y+z)

reducedRowEchelon  matrix{{1,1,0},{1,2,1}}
((submatrix(M,pivotCol))^(-1))*M

--it seems that the main functions do not work with fraction fileds


restart
installPackage "MonoSigns"
R = QQ[x,y,z]
A = matrix{{x-z},{x*z}}
reduceSystemFull(A,R)

A = matrix{{x+y},{x+z}}
reduceSystemFull(A,R)





coefficients A
viewHelp coefficients

reduceSystemFull(A,R)


check "MonoSigns"
R = QQ[x,y,z]